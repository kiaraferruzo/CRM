$.material.init()

const NOMBRE_ALERTA_SUCCESS = 'alert-success'
const NOMBRE_ALERTA_DANGER = 'alert-danger'

$(function() {

    $('[data-toggle="tooltip"]').tooltip();
    $("select").dropdown({ "autoinit" : ".select" });

    $('#content').fadeIn();
    $('#datos-familiar .contacto-card').not('.familiar-actual').addClass('card-blocked');

    $('#datos-familiar').hide();

    var $boton_editar = $('.agenda-diaria-evento .title-edit:first-of-type');
    $boton_editar.attr('data-toggle','modal');
    $boton_editar.attr('data-target','#editar-evento-agenda');

    var cal = $('#calendar').calendar();

    $('.botones-perfil .dropdown-menu li').on('click',function() {
        mostrarAlerta(NOMBRE_ALERTA_SUCCESS);
        mostrarAlerta(NOMBRE_ALERTA_DANGER);
    });

    $('#btn-crear-familiar').on('click',function() {
        $('.form-familiar').fadeIn();
        $(this).parent().find('p').fadeOut();
        $(this).fadeOut();
    });

    $('#btn-buscar-familiar').on('click',function() {
        $('#datos-familiar .contacto-row').fadeIn();
        $('#datos-familiar #guardar-parentezco').fadeIn();
    });

    $('#btn-crear-contacto').on('click',function() {
        $('.form-contactos').fadeIn();
        $(this).parent().find('p').fadeOut();
        $(this).fadeOut();
    });

    $('#btn-buscar-agregar-contacto').on('click',function() {
        $('#datos-contacto .cards-contacto-choose').fadeIn();
    });

    $('#datos-familiar .contacto-card .close').on('click',function() {
        $(this).parent('#datos-familiar .contacto-card').fadeOut();
    });

    $('.cards-contacto-choose .contacto-card').on('click',function() {
        $('.cards-contacto-choose').fadeOut();
        $('#datos-contacto .buscador-contacto-datos').fadeOut();
        $('#datos-contacto .form-contactos').fadeIn();
    });

    $('#datos-familiar .card-blocked').on('click',function() {
        $('#datos-familiar .contacto-card').not('.familiar-actual').addClass('card-blocked');
        $(this).removeClass('card-blocked');
    })

    $('#btn-buscar-page').on('click',function() {
        $('.buscar-page .show-contacts').fadeIn();
    });

    $('#filters .form-group li').on('click',function() {
        $('.full-card').fadeOut();
        $('.full-card').fadeIn();
    });

    $('.mis-contactos-page .breadcrumb li').on('click',function() {
        $('.full-card').fadeOut();
        $('.full-card').fadeIn();
    });

    // Botón borrar
    $('.agenda-diaria').on('click','.agenda-diaria-evento .title-edit:last-of-type',function() {
        $(this).parents('.agenda-diaria-evento').fadeOut();
    });

    $('.nav-tabs li').on('click',function() {
        var id_tab_pane = $(this).find('a').attr('href');
        $(id_tab_pane).siblings().hide();
        $(id_tab_pane).show();
    });

    $('.calendar').on('click','.eventos-box',function() {
        var fecha = moment($(this).data('fecha'));
        var eventos = obtenerEventos();

        $('.agenda-diaria-evento').remove();

        eventos.forEach(function(evento) {
            if (esFechaIgual(evento.fecha,fecha)) {
                var nuevo_evento_div = crearEventoAgenda(evento);
                $('.agenda-diaria .panel-body').append(nuevo_evento_div);
            }
        });

        var $boton_editar = $('.agenda-diaria-evento .title-edit:first-of-type');
        $boton_editar.attr('data-toggle','modal');
        $boton_editar.attr('data-target','#editar-evento-agenda');

        $('.agenda-diaria-evento').hide();
        $('.agenda-diaria-evento').fadeIn();

        console.log('Funciona')
    });
});

// Plugin IziToast para alertas
function mostrarAlerta(nombre_alerta) {
    if (nombre_alerta === NOMBRE_ALERTA_SUCCESS)
    {
        iziToast.success({
            title: '¡Excelente!',
            message: 'El contacto se derivó correctamente.',
            position: 'bottomLeft'
        });
    }
    else if (nombre_alerta === NOMBRE_ALERTA_DANGER)
    {
        iziToast.error({
            title: '¡Error!',
            message: 'El contacto no se pudo derivar. Inténtalo de nuevo.',
            position: 'bottomLeft'
        });
    }
}

function crearEventoAgenda(evento) {
    var evento_agenda_div = document.createElement('div');
    evento_agenda_div.classList.add('agenda-diaria-evento');

    var asunto_div = crearAsuntoEvento(evento);
    var hora_div = crearHoraEvento(evento);
    var nombre_contacto_div = crearNombreContactoEvento(evento);
    var telefono_div = crearTelefonoEvento(evento);

    evento_agenda_div.appendChild(asunto_div);
    evento_agenda_div.appendChild(hora_div);
    evento_agenda_div.appendChild(nombre_contacto_div);
    evento_agenda_div.appendChild(telefono_div);

    return evento_agenda_div;
}

function crearAsuntoEvento(evento) {
    var asunto_div = document.createElement('div');

    var title_div = document.createElement('span');
    title_div.classList.add('title');
    title_div.textContent = evento.asunto;

    var icono_editar = document.createElement('i');
    icono_editar.classList.add('material-icons','title-edit');
    icono_editar.textContent = String.fromCharCode('0xE3C9');

    var icono_borrar = document.createElement('i');
    icono_borrar.classList.add('material-icons','title-edit');
    icono_borrar.textContent = String.fromCharCode('0xE872');

    asunto_div.appendChild(title_div);
    asunto_div.appendChild(icono_editar);
    asunto_div.appendChild(icono_borrar);

    return asunto_div;
}

function crearHoraEvento(evento) {
    var hora_div = document.createElement('div');

    var icono_hora = document.createElement('i');
    icono_hora.classList.add('material-icons');
    icono_hora.textContent = String.fromCharCode('0xE192');

    var detail_div = document.createElement('span');
    detail_div.classList.add('detail');
    detail_div.textContent = evento.hora;

    hora_div.appendChild(icono_hora);
    hora_div.appendChild(detail_div);

    return hora_div;
}

function crearNombreContactoEvento(evento) {
    var nombre_contacto_div = document.createElement('div');

    var icono_contacto = document.createElement('i');
    icono_contacto.classList.add('material-icons');
    icono_contacto.textContent = String.fromCharCode('0xE7FD');

    var link_div = document.createElement('a');
    link_div.setAttribute('href','contacto-perfil.html');

    var detail_div = document.createElement('span');
    detail_div.classList.add('detail');
    detail_div.textContent = evento.nombre_contacto;

    link_div.appendChild(detail_div);

    nombre_contacto_div.appendChild(icono_contacto);
    nombre_contacto_div.appendChild(link_div);

    return nombre_contacto_div;
}

function crearTelefonoEvento(evento) {
    var telefono_div = document.createElement('div');

    var icono_telefono = document.createElement('i');
    icono_telefono.classList.add('material-icons');
    icono_telefono.textContent = String.fromCharCode('0xE0CD');

    var detail_div = document.createElement('span');
    detail_div.classList.add('detail');
    detail_div.textContent = evento.telefono;

    telefono_div.appendChild(icono_telefono);
    telefono_div.appendChild(detail_div);

    return telefono_div;
}

// Obtener eventos, data falsa para pruebas
function obtenerEventos() {
    var eventos = [
        {
                      fecha : moment('13-08-17','DD-MM-YY'),
                     asunto : 'Volver a llamar',
                       hora : 'A las 7:00',
            nombre_contacto : 'Kiara Esquerra Ferruzo',
                   telefono : 956353236
        },
        {
                      fecha : moment('18-07-17','DD-MM-YY'),
                     asunto : 'Llamar',
                       hora : 'A las 8:30',
            nombre_contacto : 'Miguel Lladó Herrera',
                   telefono : 955323234
        },
        {
                      fecha : moment('18-07-17','DD-MM-YY'),
                     asunto : 'Volver a llamar',
                       hora : 'A las 12:20',
            nombre_contacto : 'Javier Cerna Herrera',
                   telefono : 956546678
        },
        {
                      fecha : moment(),
                     asunto : 'Llamar',
                       hora : 'A las 15:45',
            nombre_contacto : 'Marcelo Osorio Nakayama',
                   telefono : 978353747
        },
        {
                      fecha : moment(),
                     asunto : 'Volver a llamar',
                       hora : 'A las 7:00',
            nombre_contacto : 'Kiara Esquerra Ferruzo',
                   telefono : 956353236
        }
    ];

    return eventos;
}

function esFechaIgual(fecha1,fecha2) {
  return fecha1.date() === fecha2.date() &&
         fecha1.month() === fecha2.month() &&
         fecha1.year() === fecha2.year()
}

// Pestaña de agenda

/*
| ------------------------------------------------------------------------------
| Calendar plugin (rough draft)
| ------------------------------------------------------------------------------
*/

(function($) {

    var Calendar = function(elem, options) {
        this.elem = elem;
        this.options = $.extend({}, Calendar.DEFAULTS, options);
        this.init();
    };

    Calendar.DEFAULTS = {
        datetime: undefined,
        dayFormat: 'DDD',
        weekFormat: 'DDD',
        monthFormat: 'MM/DD/YYYY',
        view: undefined,
    };

    Calendar.prototype.init = function() {
        if (!this.options.datetime || this.options.datetime == 'now') {
            this.options.datetime = moment();
        }
        if (!this.options.view) {
            this.options.view = 'month';
        }
        this.initScaffold()
            .initStyle()
            .render();
    }

    Calendar.prototype.initScaffold = function() {
        var $elem = $(this.elem),
            $view = $elem.find('.calendar-view'),
            $currentDate = $elem.find('.calendar-current-date');

        if (!$view.length) {
            this.view = document.createElement('div');
            this.view.className = 'calendar-view';
            this.elem.appendChild(this.view);
        } else {
            this.view = $view[0];
        }

        if ($currentDate.length > 0) {
            var dayFormat = $currentDate.data('day-format'),
                weekFormat = $currentDate.data('week-format'),
                monthFormat = $currentDate.data('month-format');
            this.currentDate = $currentDate[0];
            if (dayFormat) {
                this.options.dayFormat = dayFormat;
            }
            if (weekFormat) {
                this.options.weekFormat = weekFormat;
            }
            if (monthFormat) {
                this.options.monthFormat = monthFormat;
            }
        }
        return this;
    }

    Calendar.prototype.initStyle = function() {
        return this;
    }

    Calendar.prototype.render = function() {
        switch (this.options.view) {
            case 'month':
                this.renderMonthView(obtenerEventos());
                break;
                befault: this.renderMonth();
        }
    }

    Calendar.prototype.renderMonthView = function(eventos) {

        var datetime = this.options.datetime.clone(),
            month = datetime.month();
        datetime.startOf('month').startOf('week');

        var $view = $(this.view),
            table = document.createElement('table'),
            tbody = document.createElement('tbody');

        $view.html('');
        table.appendChild(tbody);
        table.className = 'table table-bordered';

        var week = 0,i;

        while (week < 6) {
            tr = document.createElement('tr');
            tr.className = 'calendar-month-row';
            for (i = 0; i < 7; i++) {
                td = document.createElement('td');
                td.appendChild(document.createTextNode(datetime.format('D')));
                if (month !== datetime.month()) {
                    td.className = 'calendar-prior-months-date';
                }

                var n_eventos_fecha = contarEventosEnFecha(eventos,datetime);

                if (n_eventos_fecha > 0) {
                        var eventos_box = crearEventosBox(datetime.format(),n_eventos_fecha);
                        td.appendChild(eventos_box);
                }

                if (datetime.month() === moment().month() &&
                    datetime.date() === moment().date()) {
                        td.classList.add('active-agenda-page');
                }
                tr.appendChild(td);
                datetime.add(1, 'day');
            }
            tbody.appendChild(tr);
            week++;
        }

        $view[0].appendChild(table);

        if (this.currentDate) {
            $(this.currentDate).html(
                this.options.datetime.format(this.options.monthFormat)
            );
        }

    }

    Calendar.prototype.next = function() {
        switch (this.options.view) {
            case 'month':
                this.options.datetime.endOf('month').add(1, 'day');
                this.render();
                break;
            default:
                break;
        }
    }

    Calendar.prototype.prev = function() {
        switch (this.options.view) {
            case 'month':
                this.options.datetime.startOf('month').subtract(1, 'day');
                this.render();
                break;
            default:
                break;
        }
    }

    Calendar.prototype.today = function() {
        this.options.datetime = moment();
        this.render();
    }

    function Plugin(option) {
        return this.each(function() {
            var $this = $(this),
                data = $this.data('bs.calendar'),
                options = typeof option == 'object' && option;
            if (!data) {
                data = new Calendar(this, options);
                $this.data('bs.calendar', data);
            }

            switch (option) {
                case 'today':
                    data.today();
                    break;
                case 'prev':
                    data.prev();
                    break;
                case 'next':
                    data.next();
                    break;
                default:
                    break;
            }
        });
    };

    var noConflict = $.fn.calendar;

    $.fn.calendar = Plugin;
    $.fn.calendar.Constructor = Calendar;

    $.fn.calendar.noConflict = function() {
        $.fn.calendar = noConflict;
        return this;
    };

    // Public data API.
    $('[data-toggle="calendar"]').click(function() {
        var $this = $(this),
            $elem = $this.parents('.calendar'),
            action = $this.data('action');
        if (action) {
            $elem.calendar(action);
        }
    });

    // Helpers
    function crearEventosBox(fecha,n_eventos) {
        var eventos_box_div = document.createElement('div');
        eventos_box_div.classList.add('eventos-box');

        var texto_eventos = n_eventos.toString() + (n_eventos < 2 ? ' evento' : ' eventos');
        eventos_box_div.textContent = texto_eventos;

        eventos_box_div.dataset.fecha = fecha;
        return eventos_box_div;
    }

    function contarEventosEnFecha(eventos,fecha_actual) {
        var n_eventos_fecha = 0;

        eventos.forEach(function(evento) {
            if (esFechaIgual(evento.fecha,fecha_actual)) {
                n_eventos_fecha++;
            }
        });
        return n_eventos_fecha;
    }

})(jQuery);
